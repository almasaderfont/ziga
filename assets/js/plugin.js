jQuery(document).ready(function ($) {
  $(".selectpicker").selectpicker();
  // chatScrollDirectionDown();
  uploadImg();
  initAllSiteMixItUp();
  customDropdown();
  allSiteSwiperInit();
  verificationCodeSeprate();
  showPassword($);
  toggleSideMenuInSmallScreens($);
  stickyHeader($);
  lazyLoad();
  toggleShowContacts();
});

// functions init

function lazyLoad() {
  const images = document.querySelectorAll(".lazy-omd");

  const optionsLazyLoad = {
    //  rootMargin: '-50px',
    // threshold: 1
  };

  const preloadImage = function (img) {
    img.src = img.getAttribute("data-src");
    img.onload = function () {
      img.parentElement.classList.remove("loading-omd");
      img.parentElement.classList.add("loaded-omd");
      img.parentElement.parentElement.classList.add("lazy-head-om");
    };
  };

  const imageObserver = new IntersectionObserver(function (enteries) {
    enteries.forEach(function (entery) {
      if (!entery.isIntersecting) {
        return;
      } else {
        preloadImage(entery.target);
        imageObserver.unobserve(entery.target);
      }
    });
  }, optionsLazyLoad);

  images.forEach(function (image) {
    imageObserver.observe(image);
  });
}

function swiperInit(options) {
  const swiper = new Swiper(options.className + " .swiper-container", {
    spaceBetween: 30,
    autoplay: {
      delay: 3000,
      disableOnInteraction: false,
    },
    rtl: $("html").attr("dir") === "rtl" ? true : false,
    pagination: {
      el: options.className + " .swiper-pagination",
      clickable: true,
    },
    navigation: {
      nextEl: options.className + " .swiper-button-next",
      prevEl: options.className + " .swiper-button-prev",
    },
    breakpoints: options.breakpoints,
    observer: options.observer,
    observeParents: options.observeParents,
    grid: options.grid,
    ...options,
  });

  lazyLoad();

  return swiper;
}

function allSiteSwiperInit() {
  const ourLatestSubscribersSwiperBreakNormalPoints = {
    0: {
      slidesPerView: 2,
    },
    480: {
      slidesPerView: 2,
    },
    767: {
      slidesPerView: 2,
    },
    992: {
      slidesPerView: 3,
    },
    1200: {
      slidesPerView: 4,
    },
    1800: {
      slidesPerView: 5,
    },
  };

  const ourLatestSubscribersSliderProps = {
    autoplay: false,
    className: ".our_latest_subscribers_sec__",
    breakpoints: ourLatestSubscribersSwiperBreakNormalPoints,
    spaceBetween: 10,
  };

  const sliderProps = {
    autoplay: false,
    className: ".intro_section",
    breakpoints: false,
    observer: true,
    observeParents: true,
  };

  const latestSubscribersSlider = swiperInit(ourLatestSubscribersSliderProps);
  swiperInit(sliderProps);

  return {
    reInitLatestSubscribersSlider: () => {
      latestSubscribersSlider.destroy();
      latestSubscribersSlider = swiperInit(ourLatestSubscribersSliderProps);
    },
  };
}

function verificationCodeSeprate() {
  const inputElements = [...document.querySelectorAll("input.code-input")];

  inputElements.forEach((ele, index) => {
    ele.addEventListener("keydown", (e) => {
      // if the keycode is backspace & the current field is empty
      // focus the input before the current. The event then happens
      // which will clear the input before the current
      if (e.keyCode === 8 && e.target.value === "") {
        inputElements[Math.max(0, index - 1)].focus();
      }
    });
    ele.addEventListener("input", (e) => {
      if (e.target.value === "") {
        inputElements[index].classList = "code-input";
      } else {
        inputElements[index].classList = "code-input active";
      }

      // take the first character of the input
      // this actually breaks if you input an emoji like 👨‍👩‍👧‍👦....
      // but I'm willing to overlook insane security code practices.
      const [first, ...rest] = e.target.value;
      e.target.value = first ?? ""; // the `??` '' is for the backspace usecase
      const lastInputBox = index === inputElements.length - 1;
      const insertedContent = first !== undefined;
      if (insertedContent && !lastInputBox) {
        // continue to input the rest of the string
        inputElements[index + 1].focus();
        inputElements[index + 1].value = rest.join("");
        inputElements[index + 1].dispatchEvent(new Event("input"));
      }
    });
  });
}

function showPassword($) {
  $(".show-password-button-om").on("click", function (e) {
    e.preventDefault();
    if ($(this).parent().find("input").attr("type") == "text") {
      $(this).parent().find("input").attr("type", "password");
      $(this).removeClass("show-om");
    } else {
      $(this).parent().find("input").attr("type", "text");
      $(this).addClass("show-om");
    }
  });
}

function toggleSideMenuInSmallScreens($) {
  // nav men activation
  $("#menu-butt-activ-om").on("click", function (e) {
    e.preventDefault();

    $("#navbar-menu-om").addClass("active-menu");
    $(".overlay").addClass("active");
    $("body").addClass("overflow-body");
  });

  // nav men close
  $(".close-button__ , .overlay ").on("click", function (e) {
    e.preventDefault();
    $("#navbar-menu-om").removeClass("active-menu");
    $(".overlay").removeClass("active");
    $("body").removeClass("overflow-body");
  });
}

function stickyHeader($) {
  let headerHeight = $("header").outerHeight();

  $("header").innerHeight(headerHeight);

  let lastScroll = 0;
  $(document).on("scroll", function () {
    let currentScroll = $(this).scrollTop();

    // scroll down
    if (currentScroll < lastScroll && currentScroll > headerHeight + 500) {
      // add class avtive menu
      $(".fixed_header__").addClass("active_menu__");
      $(".fixed_header__").removeClass("not_active_menu__");
    } else if (
      currentScroll > lastScroll &&
      currentScroll > headerHeight + 500
    ) {
      // scroll up
      if ($(".fixed_header__").hasClass("active_menu__")) {
        $(".fixed_header__").removeClass("active_menu__");
        $(".fixed_header__").addClass("not_active_menu__");
      }
    } else {
      $(".fixed_header__").removeClass("active_menu__");
      $(".fixed_header__").removeClass("not_active_menu__");
    }
    lastScroll = currentScroll;
  });

  $(".arrow_button__").click(() => {
    $(".side_links_section").removeClass("active");
  });
}

function customDropdown() {
  $(".dropdown_button__").on("click", function (event) {
    const perantElement = $(this).closest(".custom_dropdown__");
    const menu = perantElement.find(".dropdown_menu__");
    let timeoutId;

    event.preventDefault();
    perantElement.toggleClass("show");

    menu.on("mouseleave", function () {
      timeoutId = setTimeout(function () {
        perantElement.removeClass("show");
      }, 750);
    });

    menu.on("mouseenter", () => clearTimeout(timeoutId));
  });
}

function chatScrollDirectionDown() {
  let messageBody = document.querySelector(".messages_wrapper__");
  if (!messageBody) {
    return;
  }

  messageBody.scrollTop = messageBody.scrollHeight;
}

function mixitupInit(options) {
  // const { className, selectors: { control, target }} = options;
  const element = document.querySelector(`${options.className}`);
  if (!element) return;
  const mixitupElement = element.querySelector(".mix-container");
  const filterClassList = element.querySelector("[data-filter]").classList;
  const index = Array.from(filterClassList).indexOf("filter") + 1;
  const control = filterClassList[index];

  if (element)
    return new mixitup(mixitupElement, {
      selectors: {
        control: `.${control}`,
        target: ".mix",
      },
    });
}

function initAllSiteMixItUp() {
  const latestSubscribersOptions = {
    className: ".our_latest_subscribers_sec__",
  };

  mixitupInit(latestSubscribersOptions);
}

function uploadImg() {
  $(".upload_photo__ input").change(function (e) {
    let img = $(this).parent().find("img");
    let file = e.target.files[0];

    if (!img.length) return;

    if (file) {
      let reader = new FileReader();
      reader.onload = () => {
        img.attr("src", reader.result).show();
        img.parent().parent().addClass("uploaded__");
      };
      reader.readAsDataURL(file);
    }
  });
}

function toggleShowContacts() {
  function toggleChatContacts(e) {
    e.preventDefault();
    $(".chat_contacts__").toggleClass("active");
    $(".chat_contacts__")
      .parent()
      .find(".overlay__")
      .toggleClass("active")
      .on("click", function () {
        $(".chat_contacts__").removeClass("active");
        $(this).removeClass("active");
      });
  }

  $(".toggle_chat_contacts").on("click", (e) => toggleChatContacts(e));
  $(".close_chat_contacts").on("click", (e) => toggleChatContacts(e));
}
